/** @file
 *    @brief MAVLink comm protocol testsuite generated from flugauto.xml
 *    @see http://qgroundcontrol.org/mavlink/
 */
#pragma once
#ifndef FLUGAUTO_TESTSUITE_H
#define FLUGAUTO_TESTSUITE_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MAVLINK_TEST_ALL
#define MAVLINK_TEST_ALL
static void mavlink_test_common(uint8_t, uint8_t, mavlink_message_t *last_msg);
static void mavlink_test_flugauto(uint8_t, uint8_t, mavlink_message_t *last_msg);

static void mavlink_test_all(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
    mavlink_test_common(system_id, component_id, last_msg);
    mavlink_test_flugauto(system_id, component_id, last_msg);
}
#endif

#include "../common/testsuite.h"


static void mavlink_test_flugauto_sim_data(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
        if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA >= 256) {
            return;
        }
#endif
    mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
    mavlink_flugauto_sim_data_t packet_in = {
        93372036854775807ULL,{ 73.0, 74.0, 75.0 },{ 157.0, 158.0, 159.0 },{ 241.0, 242.0, 243.0 },{ 325.0, 326.0, 327.0 },{ 409.0, 410.0, 411.0 },{ 493.0, 494.0, 495.0 },{ 577.0, 578.0, 579.0 },{ 661.0, 662.0, 663.0 },{ 745.0, 746.0, 747.0 },{ 829.0, 830.0, 831.0 },913.0,941.0,969.0,{ 997.0, 998.0, 999.0 }
    };
    mavlink_flugauto_sim_data_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        packet1.time_usec = packet_in.time_usec;
        packet1.roll = packet_in.roll;
        packet1.pitch = packet_in.pitch;
        packet1.yaw = packet_in.yaw;
        
        mav_array_memcpy(packet1.wing_1_lift, packet_in.wing_1_lift, sizeof(float)*3);
        mav_array_memcpy(packet1.wing_2_lift, packet_in.wing_2_lift, sizeof(float)*3);
        mav_array_memcpy(packet1.wing_3_lift, packet_in.wing_3_lift, sizeof(float)*3);
        mav_array_memcpy(packet1.wing_4_lift, packet_in.wing_4_lift, sizeof(float)*3);
        mav_array_memcpy(packet1.body_lift, packet_in.body_lift, sizeof(float)*3);
        mav_array_memcpy(packet1.wing_1_drag, packet_in.wing_1_drag, sizeof(float)*3);
        mav_array_memcpy(packet1.wing_2_drag, packet_in.wing_2_drag, sizeof(float)*3);
        mav_array_memcpy(packet1.wing_3_drag, packet_in.wing_3_drag, sizeof(float)*3);
        mav_array_memcpy(packet1.wing_4_drag, packet_in.wing_4_drag, sizeof(float)*3);
        mav_array_memcpy(packet1.body_drag, packet_in.body_drag, sizeof(float)*3);
        mav_array_memcpy(packet1.airspeed, packet_in.airspeed, sizeof(float)*3);
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
        if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
           // cope with extensions
           memset(MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_MIN_LEN);
        }
#endif
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_flugauto_sim_data_encode(system_id, component_id, &msg, &packet1);
    mavlink_msg_flugauto_sim_data_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_flugauto_sim_data_pack(system_id, component_id, &msg , packet1.time_usec , packet1.wing_1_lift , packet1.wing_2_lift , packet1.wing_3_lift , packet1.wing_4_lift , packet1.body_lift , packet1.wing_1_drag , packet1.wing_2_drag , packet1.wing_3_drag , packet1.wing_4_drag , packet1.body_drag , packet1.roll , packet1.pitch , packet1.yaw , packet1.airspeed );
    mavlink_msg_flugauto_sim_data_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_flugauto_sim_data_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.time_usec , packet1.wing_1_lift , packet1.wing_2_lift , packet1.wing_3_lift , packet1.wing_4_lift , packet1.body_lift , packet1.wing_1_drag , packet1.wing_2_drag , packet1.wing_3_drag , packet1.wing_4_drag , packet1.body_drag , packet1.roll , packet1.pitch , packet1.yaw , packet1.airspeed );
    mavlink_msg_flugauto_sim_data_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
            comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
    mavlink_msg_flugauto_sim_data_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_flugauto_sim_data_send(MAVLINK_COMM_1 , packet1.time_usec , packet1.wing_1_lift , packet1.wing_2_lift , packet1.wing_3_lift , packet1.wing_4_lift , packet1.body_lift , packet1.wing_1_drag , packet1.wing_2_drag , packet1.wing_3_drag , packet1.wing_4_drag , packet1.body_drag , packet1.roll , packet1.pitch , packet1.yaw , packet1.airspeed );
    mavlink_msg_flugauto_sim_data_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_flugauto(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
    mavlink_test_flugauto_sim_data(system_id, component_id, last_msg);
}

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // FLUGAUTO_TESTSUITE_H
