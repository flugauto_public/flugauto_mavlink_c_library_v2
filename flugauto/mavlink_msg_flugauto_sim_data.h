#pragma once
// MESSAGE FLUGAUTO_SIM_DATA PACKING

#define MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA 13100

MAVPACKED(
typedef struct __mavlink_flugauto_sim_data_t {
 uint64_t time_usec; /*< [us] Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude the number.*/
 float wing_1_lift[3]; /*<  Wing 1 lift values in kg*/
 float wing_2_lift[3]; /*<  Wing 2 lift values in kg*/
 float wing_3_lift[3]; /*<  Wing 3 lift values in kg*/
 float wing_4_lift[3]; /*<  Wing 4 lift values in kg*/
 float body_lift[3]; /*<  Body lift values in kg*/
 float wing_1_drag[3]; /*<  Wing 1 drag values in kg*/
 float wing_2_drag[3]; /*<  Wing 2 drag values in kg*/
 float wing_3_drag[3]; /*<  Wing 3 drag values in kg*/
 float wing_4_drag[3]; /*<  Wing 4 drag values in kg*/
 float body_drag[3]; /*<  Body drag values in kg*/
 float roll; /*<  Roll actual*/
 float pitch; /*<  Pitch actual*/
 float yaw; /*<  Yaw actual*/
 float airspeed[3]; /*<  Airspeed XYZ*/
}) mavlink_flugauto_sim_data_t;

#define MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN 152
#define MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_MIN_LEN 152
#define MAVLINK_MSG_ID_13100_LEN 152
#define MAVLINK_MSG_ID_13100_MIN_LEN 152

#define MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_CRC 96
#define MAVLINK_MSG_ID_13100_CRC 96

#define MAVLINK_MSG_FLUGAUTO_SIM_DATA_FIELD_WING_1_LIFT_LEN 3
#define MAVLINK_MSG_FLUGAUTO_SIM_DATA_FIELD_WING_2_LIFT_LEN 3
#define MAVLINK_MSG_FLUGAUTO_SIM_DATA_FIELD_WING_3_LIFT_LEN 3
#define MAVLINK_MSG_FLUGAUTO_SIM_DATA_FIELD_WING_4_LIFT_LEN 3
#define MAVLINK_MSG_FLUGAUTO_SIM_DATA_FIELD_BODY_LIFT_LEN 3
#define MAVLINK_MSG_FLUGAUTO_SIM_DATA_FIELD_WING_1_DRAG_LEN 3
#define MAVLINK_MSG_FLUGAUTO_SIM_DATA_FIELD_WING_2_DRAG_LEN 3
#define MAVLINK_MSG_FLUGAUTO_SIM_DATA_FIELD_WING_3_DRAG_LEN 3
#define MAVLINK_MSG_FLUGAUTO_SIM_DATA_FIELD_WING_4_DRAG_LEN 3
#define MAVLINK_MSG_FLUGAUTO_SIM_DATA_FIELD_BODY_DRAG_LEN 3
#define MAVLINK_MSG_FLUGAUTO_SIM_DATA_FIELD_AIRSPEED_LEN 3

#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_FLUGAUTO_SIM_DATA { \
    13100, \
    "FLUGAUTO_SIM_DATA", \
    15, \
    {  { "time_usec", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_flugauto_sim_data_t, time_usec) }, \
         { "wing_1_lift", NULL, MAVLINK_TYPE_FLOAT, 3, 8, offsetof(mavlink_flugauto_sim_data_t, wing_1_lift) }, \
         { "wing_2_lift", NULL, MAVLINK_TYPE_FLOAT, 3, 20, offsetof(mavlink_flugauto_sim_data_t, wing_2_lift) }, \
         { "wing_3_lift", NULL, MAVLINK_TYPE_FLOAT, 3, 32, offsetof(mavlink_flugauto_sim_data_t, wing_3_lift) }, \
         { "wing_4_lift", NULL, MAVLINK_TYPE_FLOAT, 3, 44, offsetof(mavlink_flugauto_sim_data_t, wing_4_lift) }, \
         { "body_lift", NULL, MAVLINK_TYPE_FLOAT, 3, 56, offsetof(mavlink_flugauto_sim_data_t, body_lift) }, \
         { "wing_1_drag", NULL, MAVLINK_TYPE_FLOAT, 3, 68, offsetof(mavlink_flugauto_sim_data_t, wing_1_drag) }, \
         { "wing_2_drag", NULL, MAVLINK_TYPE_FLOAT, 3, 80, offsetof(mavlink_flugauto_sim_data_t, wing_2_drag) }, \
         { "wing_3_drag", NULL, MAVLINK_TYPE_FLOAT, 3, 92, offsetof(mavlink_flugauto_sim_data_t, wing_3_drag) }, \
         { "wing_4_drag", NULL, MAVLINK_TYPE_FLOAT, 3, 104, offsetof(mavlink_flugauto_sim_data_t, wing_4_drag) }, \
         { "body_drag", NULL, MAVLINK_TYPE_FLOAT, 3, 116, offsetof(mavlink_flugauto_sim_data_t, body_drag) }, \
         { "roll", NULL, MAVLINK_TYPE_FLOAT, 0, 128, offsetof(mavlink_flugauto_sim_data_t, roll) }, \
         { "pitch", NULL, MAVLINK_TYPE_FLOAT, 0, 132, offsetof(mavlink_flugauto_sim_data_t, pitch) }, \
         { "yaw", NULL, MAVLINK_TYPE_FLOAT, 0, 136, offsetof(mavlink_flugauto_sim_data_t, yaw) }, \
         { "airspeed", NULL, MAVLINK_TYPE_FLOAT, 3, 140, offsetof(mavlink_flugauto_sim_data_t, airspeed) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_FLUGAUTO_SIM_DATA { \
    "FLUGAUTO_SIM_DATA", \
    15, \
    {  { "time_usec", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_flugauto_sim_data_t, time_usec) }, \
         { "wing_1_lift", NULL, MAVLINK_TYPE_FLOAT, 3, 8, offsetof(mavlink_flugauto_sim_data_t, wing_1_lift) }, \
         { "wing_2_lift", NULL, MAVLINK_TYPE_FLOAT, 3, 20, offsetof(mavlink_flugauto_sim_data_t, wing_2_lift) }, \
         { "wing_3_lift", NULL, MAVLINK_TYPE_FLOAT, 3, 32, offsetof(mavlink_flugauto_sim_data_t, wing_3_lift) }, \
         { "wing_4_lift", NULL, MAVLINK_TYPE_FLOAT, 3, 44, offsetof(mavlink_flugauto_sim_data_t, wing_4_lift) }, \
         { "body_lift", NULL, MAVLINK_TYPE_FLOAT, 3, 56, offsetof(mavlink_flugauto_sim_data_t, body_lift) }, \
         { "wing_1_drag", NULL, MAVLINK_TYPE_FLOAT, 3, 68, offsetof(mavlink_flugauto_sim_data_t, wing_1_drag) }, \
         { "wing_2_drag", NULL, MAVLINK_TYPE_FLOAT, 3, 80, offsetof(mavlink_flugauto_sim_data_t, wing_2_drag) }, \
         { "wing_3_drag", NULL, MAVLINK_TYPE_FLOAT, 3, 92, offsetof(mavlink_flugauto_sim_data_t, wing_3_drag) }, \
         { "wing_4_drag", NULL, MAVLINK_TYPE_FLOAT, 3, 104, offsetof(mavlink_flugauto_sim_data_t, wing_4_drag) }, \
         { "body_drag", NULL, MAVLINK_TYPE_FLOAT, 3, 116, offsetof(mavlink_flugauto_sim_data_t, body_drag) }, \
         { "roll", NULL, MAVLINK_TYPE_FLOAT, 0, 128, offsetof(mavlink_flugauto_sim_data_t, roll) }, \
         { "pitch", NULL, MAVLINK_TYPE_FLOAT, 0, 132, offsetof(mavlink_flugauto_sim_data_t, pitch) }, \
         { "yaw", NULL, MAVLINK_TYPE_FLOAT, 0, 136, offsetof(mavlink_flugauto_sim_data_t, yaw) }, \
         { "airspeed", NULL, MAVLINK_TYPE_FLOAT, 3, 140, offsetof(mavlink_flugauto_sim_data_t, airspeed) }, \
         } \
}
#endif

/**
 * @brief Pack a flugauto_sim_data message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time_usec [us] Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude the number.
 * @param wing_1_lift  Wing 1 lift values in kg
 * @param wing_2_lift  Wing 2 lift values in kg
 * @param wing_3_lift  Wing 3 lift values in kg
 * @param wing_4_lift  Wing 4 lift values in kg
 * @param body_lift  Body lift values in kg
 * @param wing_1_drag  Wing 1 drag values in kg
 * @param wing_2_drag  Wing 2 drag values in kg
 * @param wing_3_drag  Wing 3 drag values in kg
 * @param wing_4_drag  Wing 4 drag values in kg
 * @param body_drag  Body drag values in kg
 * @param roll  Roll actual
 * @param pitch  Pitch actual
 * @param yaw  Yaw actual
 * @param airspeed  Airspeed XYZ
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint64_t time_usec, const float *wing_1_lift, const float *wing_2_lift, const float *wing_3_lift, const float *wing_4_lift, const float *body_lift, const float *wing_1_drag, const float *wing_2_drag, const float *wing_3_drag, const float *wing_4_drag, const float *body_drag, float roll, float pitch, float yaw, const float *airspeed)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN];
    _mav_put_uint64_t(buf, 0, time_usec);
    _mav_put_float(buf, 128, roll);
    _mav_put_float(buf, 132, pitch);
    _mav_put_float(buf, 136, yaw);
    _mav_put_float_array(buf, 8, wing_1_lift, 3);
    _mav_put_float_array(buf, 20, wing_2_lift, 3);
    _mav_put_float_array(buf, 32, wing_3_lift, 3);
    _mav_put_float_array(buf, 44, wing_4_lift, 3);
    _mav_put_float_array(buf, 56, body_lift, 3);
    _mav_put_float_array(buf, 68, wing_1_drag, 3);
    _mav_put_float_array(buf, 80, wing_2_drag, 3);
    _mav_put_float_array(buf, 92, wing_3_drag, 3);
    _mav_put_float_array(buf, 104, wing_4_drag, 3);
    _mav_put_float_array(buf, 116, body_drag, 3);
    _mav_put_float_array(buf, 140, airspeed, 3);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN);
#else
    mavlink_flugauto_sim_data_t packet;
    packet.time_usec = time_usec;
    packet.roll = roll;
    packet.pitch = pitch;
    packet.yaw = yaw;
    mav_array_memcpy(packet.wing_1_lift, wing_1_lift, sizeof(float)*3);
    mav_array_memcpy(packet.wing_2_lift, wing_2_lift, sizeof(float)*3);
    mav_array_memcpy(packet.wing_3_lift, wing_3_lift, sizeof(float)*3);
    mav_array_memcpy(packet.wing_4_lift, wing_4_lift, sizeof(float)*3);
    mav_array_memcpy(packet.body_lift, body_lift, sizeof(float)*3);
    mav_array_memcpy(packet.wing_1_drag, wing_1_drag, sizeof(float)*3);
    mav_array_memcpy(packet.wing_2_drag, wing_2_drag, sizeof(float)*3);
    mav_array_memcpy(packet.wing_3_drag, wing_3_drag, sizeof(float)*3);
    mav_array_memcpy(packet.wing_4_drag, wing_4_drag, sizeof(float)*3);
    mav_array_memcpy(packet.body_drag, body_drag, sizeof(float)*3);
    mav_array_memcpy(packet.airspeed, airspeed, sizeof(float)*3);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_MIN_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_CRC);
}

/**
 * @brief Pack a flugauto_sim_data message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time_usec [us] Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude the number.
 * @param wing_1_lift  Wing 1 lift values in kg
 * @param wing_2_lift  Wing 2 lift values in kg
 * @param wing_3_lift  Wing 3 lift values in kg
 * @param wing_4_lift  Wing 4 lift values in kg
 * @param body_lift  Body lift values in kg
 * @param wing_1_drag  Wing 1 drag values in kg
 * @param wing_2_drag  Wing 2 drag values in kg
 * @param wing_3_drag  Wing 3 drag values in kg
 * @param wing_4_drag  Wing 4 drag values in kg
 * @param body_drag  Body drag values in kg
 * @param roll  Roll actual
 * @param pitch  Pitch actual
 * @param yaw  Yaw actual
 * @param airspeed  Airspeed XYZ
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint64_t time_usec,const float *wing_1_lift,const float *wing_2_lift,const float *wing_3_lift,const float *wing_4_lift,const float *body_lift,const float *wing_1_drag,const float *wing_2_drag,const float *wing_3_drag,const float *wing_4_drag,const float *body_drag,float roll,float pitch,float yaw,const float *airspeed)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN];
    _mav_put_uint64_t(buf, 0, time_usec);
    _mav_put_float(buf, 128, roll);
    _mav_put_float(buf, 132, pitch);
    _mav_put_float(buf, 136, yaw);
    _mav_put_float_array(buf, 8, wing_1_lift, 3);
    _mav_put_float_array(buf, 20, wing_2_lift, 3);
    _mav_put_float_array(buf, 32, wing_3_lift, 3);
    _mav_put_float_array(buf, 44, wing_4_lift, 3);
    _mav_put_float_array(buf, 56, body_lift, 3);
    _mav_put_float_array(buf, 68, wing_1_drag, 3);
    _mav_put_float_array(buf, 80, wing_2_drag, 3);
    _mav_put_float_array(buf, 92, wing_3_drag, 3);
    _mav_put_float_array(buf, 104, wing_4_drag, 3);
    _mav_put_float_array(buf, 116, body_drag, 3);
    _mav_put_float_array(buf, 140, airspeed, 3);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN);
#else
    mavlink_flugauto_sim_data_t packet;
    packet.time_usec = time_usec;
    packet.roll = roll;
    packet.pitch = pitch;
    packet.yaw = yaw;
    mav_array_memcpy(packet.wing_1_lift, wing_1_lift, sizeof(float)*3);
    mav_array_memcpy(packet.wing_2_lift, wing_2_lift, sizeof(float)*3);
    mav_array_memcpy(packet.wing_3_lift, wing_3_lift, sizeof(float)*3);
    mav_array_memcpy(packet.wing_4_lift, wing_4_lift, sizeof(float)*3);
    mav_array_memcpy(packet.body_lift, body_lift, sizeof(float)*3);
    mav_array_memcpy(packet.wing_1_drag, wing_1_drag, sizeof(float)*3);
    mav_array_memcpy(packet.wing_2_drag, wing_2_drag, sizeof(float)*3);
    mav_array_memcpy(packet.wing_3_drag, wing_3_drag, sizeof(float)*3);
    mav_array_memcpy(packet.wing_4_drag, wing_4_drag, sizeof(float)*3);
    mav_array_memcpy(packet.body_drag, body_drag, sizeof(float)*3);
    mav_array_memcpy(packet.airspeed, airspeed, sizeof(float)*3);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_MIN_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_CRC);
}

/**
 * @brief Encode a flugauto_sim_data struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param flugauto_sim_data C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_flugauto_sim_data_t* flugauto_sim_data)
{
    return mavlink_msg_flugauto_sim_data_pack(system_id, component_id, msg, flugauto_sim_data->time_usec, flugauto_sim_data->wing_1_lift, flugauto_sim_data->wing_2_lift, flugauto_sim_data->wing_3_lift, flugauto_sim_data->wing_4_lift, flugauto_sim_data->body_lift, flugauto_sim_data->wing_1_drag, flugauto_sim_data->wing_2_drag, flugauto_sim_data->wing_3_drag, flugauto_sim_data->wing_4_drag, flugauto_sim_data->body_drag, flugauto_sim_data->roll, flugauto_sim_data->pitch, flugauto_sim_data->yaw, flugauto_sim_data->airspeed);
}

/**
 * @brief Encode a flugauto_sim_data struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param flugauto_sim_data C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_flugauto_sim_data_t* flugauto_sim_data)
{
    return mavlink_msg_flugauto_sim_data_pack_chan(system_id, component_id, chan, msg, flugauto_sim_data->time_usec, flugauto_sim_data->wing_1_lift, flugauto_sim_data->wing_2_lift, flugauto_sim_data->wing_3_lift, flugauto_sim_data->wing_4_lift, flugauto_sim_data->body_lift, flugauto_sim_data->wing_1_drag, flugauto_sim_data->wing_2_drag, flugauto_sim_data->wing_3_drag, flugauto_sim_data->wing_4_drag, flugauto_sim_data->body_drag, flugauto_sim_data->roll, flugauto_sim_data->pitch, flugauto_sim_data->yaw, flugauto_sim_data->airspeed);
}

/**
 * @brief Send a flugauto_sim_data message
 * @param chan MAVLink channel to send the message
 *
 * @param time_usec [us] Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude the number.
 * @param wing_1_lift  Wing 1 lift values in kg
 * @param wing_2_lift  Wing 2 lift values in kg
 * @param wing_3_lift  Wing 3 lift values in kg
 * @param wing_4_lift  Wing 4 lift values in kg
 * @param body_lift  Body lift values in kg
 * @param wing_1_drag  Wing 1 drag values in kg
 * @param wing_2_drag  Wing 2 drag values in kg
 * @param wing_3_drag  Wing 3 drag values in kg
 * @param wing_4_drag  Wing 4 drag values in kg
 * @param body_drag  Body drag values in kg
 * @param roll  Roll actual
 * @param pitch  Pitch actual
 * @param yaw  Yaw actual
 * @param airspeed  Airspeed XYZ
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_flugauto_sim_data_send(mavlink_channel_t chan, uint64_t time_usec, const float *wing_1_lift, const float *wing_2_lift, const float *wing_3_lift, const float *wing_4_lift, const float *body_lift, const float *wing_1_drag, const float *wing_2_drag, const float *wing_3_drag, const float *wing_4_drag, const float *body_drag, float roll, float pitch, float yaw, const float *airspeed)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN];
    _mav_put_uint64_t(buf, 0, time_usec);
    _mav_put_float(buf, 128, roll);
    _mav_put_float(buf, 132, pitch);
    _mav_put_float(buf, 136, yaw);
    _mav_put_float_array(buf, 8, wing_1_lift, 3);
    _mav_put_float_array(buf, 20, wing_2_lift, 3);
    _mav_put_float_array(buf, 32, wing_3_lift, 3);
    _mav_put_float_array(buf, 44, wing_4_lift, 3);
    _mav_put_float_array(buf, 56, body_lift, 3);
    _mav_put_float_array(buf, 68, wing_1_drag, 3);
    _mav_put_float_array(buf, 80, wing_2_drag, 3);
    _mav_put_float_array(buf, 92, wing_3_drag, 3);
    _mav_put_float_array(buf, 104, wing_4_drag, 3);
    _mav_put_float_array(buf, 116, body_drag, 3);
    _mav_put_float_array(buf, 140, airspeed, 3);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA, buf, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_MIN_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_CRC);
#else
    mavlink_flugauto_sim_data_t packet;
    packet.time_usec = time_usec;
    packet.roll = roll;
    packet.pitch = pitch;
    packet.yaw = yaw;
    mav_array_memcpy(packet.wing_1_lift, wing_1_lift, sizeof(float)*3);
    mav_array_memcpy(packet.wing_2_lift, wing_2_lift, sizeof(float)*3);
    mav_array_memcpy(packet.wing_3_lift, wing_3_lift, sizeof(float)*3);
    mav_array_memcpy(packet.wing_4_lift, wing_4_lift, sizeof(float)*3);
    mav_array_memcpy(packet.body_lift, body_lift, sizeof(float)*3);
    mav_array_memcpy(packet.wing_1_drag, wing_1_drag, sizeof(float)*3);
    mav_array_memcpy(packet.wing_2_drag, wing_2_drag, sizeof(float)*3);
    mav_array_memcpy(packet.wing_3_drag, wing_3_drag, sizeof(float)*3);
    mav_array_memcpy(packet.wing_4_drag, wing_4_drag, sizeof(float)*3);
    mav_array_memcpy(packet.body_drag, body_drag, sizeof(float)*3);
    mav_array_memcpy(packet.airspeed, airspeed, sizeof(float)*3);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA, (const char *)&packet, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_MIN_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_CRC);
#endif
}

/**
 * @brief Send a flugauto_sim_data message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_flugauto_sim_data_send_struct(mavlink_channel_t chan, const mavlink_flugauto_sim_data_t* flugauto_sim_data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_flugauto_sim_data_send(chan, flugauto_sim_data->time_usec, flugauto_sim_data->wing_1_lift, flugauto_sim_data->wing_2_lift, flugauto_sim_data->wing_3_lift, flugauto_sim_data->wing_4_lift, flugauto_sim_data->body_lift, flugauto_sim_data->wing_1_drag, flugauto_sim_data->wing_2_drag, flugauto_sim_data->wing_3_drag, flugauto_sim_data->wing_4_drag, flugauto_sim_data->body_drag, flugauto_sim_data->roll, flugauto_sim_data->pitch, flugauto_sim_data->yaw, flugauto_sim_data->airspeed);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA, (const char *)flugauto_sim_data, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_MIN_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_CRC);
#endif
}

#if MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_flugauto_sim_data_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint64_t time_usec, const float *wing_1_lift, const float *wing_2_lift, const float *wing_3_lift, const float *wing_4_lift, const float *body_lift, const float *wing_1_drag, const float *wing_2_drag, const float *wing_3_drag, const float *wing_4_drag, const float *body_drag, float roll, float pitch, float yaw, const float *airspeed)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, time_usec);
    _mav_put_float(buf, 128, roll);
    _mav_put_float(buf, 132, pitch);
    _mav_put_float(buf, 136, yaw);
    _mav_put_float_array(buf, 8, wing_1_lift, 3);
    _mav_put_float_array(buf, 20, wing_2_lift, 3);
    _mav_put_float_array(buf, 32, wing_3_lift, 3);
    _mav_put_float_array(buf, 44, wing_4_lift, 3);
    _mav_put_float_array(buf, 56, body_lift, 3);
    _mav_put_float_array(buf, 68, wing_1_drag, 3);
    _mav_put_float_array(buf, 80, wing_2_drag, 3);
    _mav_put_float_array(buf, 92, wing_3_drag, 3);
    _mav_put_float_array(buf, 104, wing_4_drag, 3);
    _mav_put_float_array(buf, 116, body_drag, 3);
    _mav_put_float_array(buf, 140, airspeed, 3);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA, buf, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_MIN_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_CRC);
#else
    mavlink_flugauto_sim_data_t *packet = (mavlink_flugauto_sim_data_t *)msgbuf;
    packet->time_usec = time_usec;
    packet->roll = roll;
    packet->pitch = pitch;
    packet->yaw = yaw;
    mav_array_memcpy(packet->wing_1_lift, wing_1_lift, sizeof(float)*3);
    mav_array_memcpy(packet->wing_2_lift, wing_2_lift, sizeof(float)*3);
    mav_array_memcpy(packet->wing_3_lift, wing_3_lift, sizeof(float)*3);
    mav_array_memcpy(packet->wing_4_lift, wing_4_lift, sizeof(float)*3);
    mav_array_memcpy(packet->body_lift, body_lift, sizeof(float)*3);
    mav_array_memcpy(packet->wing_1_drag, wing_1_drag, sizeof(float)*3);
    mav_array_memcpy(packet->wing_2_drag, wing_2_drag, sizeof(float)*3);
    mav_array_memcpy(packet->wing_3_drag, wing_3_drag, sizeof(float)*3);
    mav_array_memcpy(packet->wing_4_drag, wing_4_drag, sizeof(float)*3);
    mav_array_memcpy(packet->body_drag, body_drag, sizeof(float)*3);
    mav_array_memcpy(packet->airspeed, airspeed, sizeof(float)*3);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA, (const char *)packet, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_MIN_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_CRC);
#endif
}
#endif

#endif

// MESSAGE FLUGAUTO_SIM_DATA UNPACKING


/**
 * @brief Get field time_usec from flugauto_sim_data message
 *
 * @return [us] Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude the number.
 */
static inline uint64_t mavlink_msg_flugauto_sim_data_get_time_usec(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Get field wing_1_lift from flugauto_sim_data message
 *
 * @return  Wing 1 lift values in kg
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_get_wing_1_lift(const mavlink_message_t* msg, float *wing_1_lift)
{
    return _MAV_RETURN_float_array(msg, wing_1_lift, 3,  8);
}

/**
 * @brief Get field wing_2_lift from flugauto_sim_data message
 *
 * @return  Wing 2 lift values in kg
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_get_wing_2_lift(const mavlink_message_t* msg, float *wing_2_lift)
{
    return _MAV_RETURN_float_array(msg, wing_2_lift, 3,  20);
}

/**
 * @brief Get field wing_3_lift from flugauto_sim_data message
 *
 * @return  Wing 3 lift values in kg
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_get_wing_3_lift(const mavlink_message_t* msg, float *wing_3_lift)
{
    return _MAV_RETURN_float_array(msg, wing_3_lift, 3,  32);
}

/**
 * @brief Get field wing_4_lift from flugauto_sim_data message
 *
 * @return  Wing 4 lift values in kg
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_get_wing_4_lift(const mavlink_message_t* msg, float *wing_4_lift)
{
    return _MAV_RETURN_float_array(msg, wing_4_lift, 3,  44);
}

/**
 * @brief Get field body_lift from flugauto_sim_data message
 *
 * @return  Body lift values in kg
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_get_body_lift(const mavlink_message_t* msg, float *body_lift)
{
    return _MAV_RETURN_float_array(msg, body_lift, 3,  56);
}

/**
 * @brief Get field wing_1_drag from flugauto_sim_data message
 *
 * @return  Wing 1 drag values in kg
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_get_wing_1_drag(const mavlink_message_t* msg, float *wing_1_drag)
{
    return _MAV_RETURN_float_array(msg, wing_1_drag, 3,  68);
}

/**
 * @brief Get field wing_2_drag from flugauto_sim_data message
 *
 * @return  Wing 2 drag values in kg
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_get_wing_2_drag(const mavlink_message_t* msg, float *wing_2_drag)
{
    return _MAV_RETURN_float_array(msg, wing_2_drag, 3,  80);
}

/**
 * @brief Get field wing_3_drag from flugauto_sim_data message
 *
 * @return  Wing 3 drag values in kg
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_get_wing_3_drag(const mavlink_message_t* msg, float *wing_3_drag)
{
    return _MAV_RETURN_float_array(msg, wing_3_drag, 3,  92);
}

/**
 * @brief Get field wing_4_drag from flugauto_sim_data message
 *
 * @return  Wing 4 drag values in kg
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_get_wing_4_drag(const mavlink_message_t* msg, float *wing_4_drag)
{
    return _MAV_RETURN_float_array(msg, wing_4_drag, 3,  104);
}

/**
 * @brief Get field body_drag from flugauto_sim_data message
 *
 * @return  Body drag values in kg
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_get_body_drag(const mavlink_message_t* msg, float *body_drag)
{
    return _MAV_RETURN_float_array(msg, body_drag, 3,  116);
}

/**
 * @brief Get field roll from flugauto_sim_data message
 *
 * @return  Roll actual
 */
static inline float mavlink_msg_flugauto_sim_data_get_roll(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  128);
}

/**
 * @brief Get field pitch from flugauto_sim_data message
 *
 * @return  Pitch actual
 */
static inline float mavlink_msg_flugauto_sim_data_get_pitch(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  132);
}

/**
 * @brief Get field yaw from flugauto_sim_data message
 *
 * @return  Yaw actual
 */
static inline float mavlink_msg_flugauto_sim_data_get_yaw(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  136);
}

/**
 * @brief Get field airspeed from flugauto_sim_data message
 *
 * @return  Airspeed XYZ
 */
static inline uint16_t mavlink_msg_flugauto_sim_data_get_airspeed(const mavlink_message_t* msg, float *airspeed)
{
    return _MAV_RETURN_float_array(msg, airspeed, 3,  140);
}

/**
 * @brief Decode a flugauto_sim_data message into a struct
 *
 * @param msg The message to decode
 * @param flugauto_sim_data C-struct to decode the message contents into
 */
static inline void mavlink_msg_flugauto_sim_data_decode(const mavlink_message_t* msg, mavlink_flugauto_sim_data_t* flugauto_sim_data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    flugauto_sim_data->time_usec = mavlink_msg_flugauto_sim_data_get_time_usec(msg);
    mavlink_msg_flugauto_sim_data_get_wing_1_lift(msg, flugauto_sim_data->wing_1_lift);
    mavlink_msg_flugauto_sim_data_get_wing_2_lift(msg, flugauto_sim_data->wing_2_lift);
    mavlink_msg_flugauto_sim_data_get_wing_3_lift(msg, flugauto_sim_data->wing_3_lift);
    mavlink_msg_flugauto_sim_data_get_wing_4_lift(msg, flugauto_sim_data->wing_4_lift);
    mavlink_msg_flugauto_sim_data_get_body_lift(msg, flugauto_sim_data->body_lift);
    mavlink_msg_flugauto_sim_data_get_wing_1_drag(msg, flugauto_sim_data->wing_1_drag);
    mavlink_msg_flugauto_sim_data_get_wing_2_drag(msg, flugauto_sim_data->wing_2_drag);
    mavlink_msg_flugauto_sim_data_get_wing_3_drag(msg, flugauto_sim_data->wing_3_drag);
    mavlink_msg_flugauto_sim_data_get_wing_4_drag(msg, flugauto_sim_data->wing_4_drag);
    mavlink_msg_flugauto_sim_data_get_body_drag(msg, flugauto_sim_data->body_drag);
    flugauto_sim_data->roll = mavlink_msg_flugauto_sim_data_get_roll(msg);
    flugauto_sim_data->pitch = mavlink_msg_flugauto_sim_data_get_pitch(msg);
    flugauto_sim_data->yaw = mavlink_msg_flugauto_sim_data_get_yaw(msg);
    mavlink_msg_flugauto_sim_data_get_airspeed(msg, flugauto_sim_data->airspeed);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN? msg->len : MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN;
        memset(flugauto_sim_data, 0, MAVLINK_MSG_ID_FLUGAUTO_SIM_DATA_LEN);
    memcpy(flugauto_sim_data, _MAV_PAYLOAD(msg), len);
#endif
}
